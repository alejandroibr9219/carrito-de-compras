<?php
require_once 'models/categoria.php';
require_once 'models/producto.php';
class categoriaController{
    
    public function index(){
        Utils::isAdmin();
        $categoria = new Categoria();
        $categorias = $categoria->getAll();
        require_once 'views/categoria/index.php';
    }
    public function  crear(){
        Utils::isAdmin();
        require_once 'views/categoria/crear.php';
    }
    public function ver(){
        if(isset($_GET['id'])){
           $id= $_GET['id'];
           
           $categoria= new Categoria();
           $categoria->setId($id);        
           $categoria = $categoria->getOne();
            
            //var_dump($categoria);
           
           //Conseguir Productos
           $producto = new Producto();
           $producto->setCategoria_id($id);
           $producto = $producto->getAllCategoria();
        }
        
         require_once 'views/categoria/ver.php';
    }

        public function guardar(){
        Utils::isAdmin();
        
        if(isset($_POST) && isset($_POST['nombre'])){
            $categoria= new Categoria();
            $categoria->setNombre($_POST['nombre']);
            $categoria->guardar();
        }
        
        header("location:".base_url."categoria/index");
      
    }
}

