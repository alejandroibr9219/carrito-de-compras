<h1>Gestion de Productos</h1>
<?php if(isset($_SESSION['producto']) && $_SESSION['producto'] == 'Complete'):?>
<strong class="alert_green">Producto Creado</strong>
<?php elseif(isset($_SESSION['producto']) && $_SESSION['producto'] == 'failed'): ?>
<strong class="alert_red">Producto Fallido, introduce bien los datos</strong>
<?php endif; ?>
<?php Utils::deleteSession('producto'); ?>

<?php if(isset($_SESSION['delete']) && $_SESSION['delete'] == 'Complete'):?>
<strong class="alert_green">Producto Eliminado</strong>
<?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
<strong class="alert_red">Producto no se Pudo eliminar</strong>
<?php endif; ?>
<?php Utils::deleteSession('delete'); ?>


<a href="<?=base_url?>producto/crear" class="button button-small">
Crear Productos
</a>
<table >
    <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>PRECIO</th>
        <th>STOCK</th>
        <th>ACCIONES</th>
    </tr>
        <?php while ($pro = $producto -> fetch_object()): ?>
    <tr>
        <td><?= $pro -> id; ?></td>
        <td><?= $pro -> nombre; ?></td>
        <td><?= $pro -> precio; ?></td>
        <td><?= $pro -> stock; ?></td>
        <td>
            <a href="<?=base_url?>producto/editar&id=<?=$pro->id?>" class="button button-gestion">Editar</a>
             <a href="<?=base_url?>producto/eliminar&id=<?=$pro->id?>" class="button button-gestion button-red">Eliminar</a>
            
        </td>
    </tr>
        <?php endwhile; ?>
</table>
